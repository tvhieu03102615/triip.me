"use strict";

jQuery(document).ready(function($) {
    // Window on load
    // //typing
    // var typed = new Typed('span.testtyping.testtyping-wrap', {
    //   strings: ['Some strings without', 'Some HTML', 'Chars'],
    //   loop: true
    // });
    //typing
    //typing-end
    $(window).on('load', function() {});
    if ($('#typed').length != 0) {
        var typed = new Typed('#typed', {
            strings: ["First content.", "Second content."],
            typeSpeed: 50,
            loop: true
        });
    }
    if ($('input.search-field').length != 0) {
        $('input.search-field').click(function() {
            $('.place-box .over-play').addClass('active');
            $('.search-suggestion-list').addClass('active');
        });
        $('.place-box .over-play').not('.place-box__content').click(function() {
            $('.search-suggestion-list').removeClass('active');
            $('.place-box .over-play').removeClass('active');
        });
    }
    if ($('button.participant-button').length != 0) {
        $('button.participant-button').click(function(e) {
            e.preventDefault();
            $('ul.participant__list').addClass('active');
            $('.search-component .over-play').addClass('active');
        });
        $('.search-component .over-play').not('.place-box__content').click(function() {
            $('ul.participant__list').removeClass('active');
            $('.search-component .over-play').removeClass('active');
        });
    }
    $('.filterDate .input-group.date').datepicker({
        format: "dd.mm.yyyy"
    });

    $('body').each(function(e, el) {
        if ($(el).find('.header-fixed').length >= 1)
            $(el).find('header').addClass('active');
        else
            $(el).find('header').removeClass('active');
    })

    //validate
    $("#signin-form").validate({
      rules: {
        password: {
          required: true,
          minlength: 6

        }
      }
    });
    $("#register-form").validate({
      rules: {
        password: {
          required: true,
          minlength: 6

        },
        firstname:{
          minlength: 1
        },
        lastname:{
          minlength: 1
        }
      }
    });
    $("#booking-form").validate({
        rules: {
            field: {
                required: true,
                number: true
            }
        }
    });
    var Info = $('.single-page .info').position();
    var TourInformation = $('.single-page .tour-information').position();
    var Itinerary = $('.single-page .itinerary').position();
    var Review = $('.single-page .review').position();
    if($('.header-fixed').length != 0){
      $(window).scroll(function(){
        var topPos = $(this).scrollTop();
        if(topPos > 530){
          $('.single-page .book').addClass('fixed');
          $('.header-fixed').addClass('fixed');
        }
        else{
          $('.single-page .book').removeClass('fixed');
          $('.header-fixed').removeClass('fixed');
        }
        if($(window).width()<993&&topPos>530){
          $(".btn-requestmobile").css("display","block");
        }else{
          $(".btn-requestmobile").css("display","none");
        }

        if(topPos > Info.top - 200 && topPos < TourInformation.top + 400){
          $('.header-fixed li.Special').addClass('active');
        }
        else{
          $('.header-fixed li.Special').removeClass('active');
        }
        if(topPos > TourInformation.top + 400 && topPos < Itinerary.top + 400){
          $('.header-fixed li.Information').addClass('active');
        }
        else{
          $('.header-fixed li.Information').removeClass('active');
        }
        if(topPos > Itinerary.top + 400 && topPos < Review.top + 400){
          $('.header-fixed li.Itinerary').addClass('active');
        }
        else{
          $('.header-fixed li.Itinerary').removeClass('active');
        }
        if(topPos > Review.top + 400){
          $('.header-fixed li.Reviews').addClass('active');
        }
        else{
          $('.header-fixed li.Reviews').removeClass('active');
        }
      });
    }
    $('nav.menu-fixed li.Special').click(function(){
      $('html,body').animate({
        scrollTop: Info.top + 400
      },1200);
      return false;
    });
    $('nav.menu-fixed li.Information').click(function(){
      $('html,body').animate({
        scrollTop: TourInformation.top + 500
      },400);
      return false;
    });
    $('nav.menu-fixed li.Itinerary').click(function(){
      $('html,body').animate({
        scrollTop: Itinerary.top + 500
      },1200);
      return false;
    });
    $('nav.menu-fixed li.Reviews').click(function(){
      $('html,body').animate({
        scrollTop: Review.top + 700
      },1200);
      return false;
    });
    function computeroom(e,state,type){
      var value=parseInt(e.siblings("span").text());
      if(state){
        if(type=="children"){
          if(value>0){
            value--;
          }
        }else{
          if(value>1){
            value--;
          }
        }
      }else{
        if(type=="children"){
          if(value<10){
            value++;
          }
        }else{
          if(value<30){
            value++;
          }
        }
      }
      e.siblings("span").text(value);
      $("button.participant-button").html('<i class="fas fa-users"></i>'+(parseInt($(".children span").text())+parseInt($(".adults span").text()))+' guest · '+$(".room span").text()+' room');
      $("input[name='people']").val((parseInt($(".children span").text())+parseInt($(".adults span").text())));
    }
    $("button.pre").on("click",function(re){
      computeroom($(this),true,$(this).closest("li").attr("class"));
      
      re.preventDefault();
      
    });
    $("button.add").on("click",function(re){
      computeroom($(this),false,$(this).closest("li").attr("class"));
      re.preventDefault();
      
    });
    // $().fancybox({
    //   selector : '["book-bottom__act"]',
    //   loop     : true
    // });
    $('#search-field').selectize( );
    $('#Enjoy-box').selectize( );


    //password show
    $('.hide-show').show();
    $('.hide-show a').addClass('show')
    
    $('.hide-show a').click(function(e){
      if( $(this).hasClass('show') ) {
        $(this).text('Hide');
        $('input[name="password"]').attr('type','text');
        $(this).removeClass('show');
      } else {
          $(this).text('Show');
          $('input[name="password"]').attr('type','password');
          $(this).addClass('show');
      }
      e.preventDefault();
    });
    
    $('form button[type="submit"]').on('click', function(e){
      $('.hide-show a').text('Show').addClass('show');
      $('.hide-show').parent().find('input[name="password"]').attr('type','password');
    }); 

    //fancy box
    $("[data-signin]").fancybox({
      arrows: false,
      infobar: false,
      smallBtn: true
    });
    $("[data-register]").fancybox({
      arrows: false,
      infobar: false,
      smallBtn: true
    });
    $("span.btn-requestmobile").click(function(){
      $(".book").slideToggle();
    });
    $(".book .fas.fa-times").click(function(){
      $(".book").slideUp();
    });
    $(window).resize(function(){
      var currentWidth=$(window).width();
      var topPos = $(window).scrollTop();
      if(currentWidth>991||topPos<531){
        $(".btn-requestmobile").css("display","none");
      }else if(currentWidth<992&&topPos>530){
        $(".btn-requestmobile").css("display","block");
      }
      if(currentWidth>991){
        $(".book").css("display","block");
      }else{
        $(".book").css("display","none");
      }
    });
    $(".group-tabs .nav.nav-tabs li a").click(function(){
      if($(this).parent().attr("class")!="active"){
        if($(this).attr("aria-controls")=="profile"){
          $(".tab-content #register").css("display","block");
          $(".tab-content #signin").css("display","none");
        }else{
          $(".tab-content #register").css("display","none");
          $(".tab-content #signin").css("display","block");
        }
      }
    });
    $(".right-header a[data-register]").click(function(){
      $(".tab-content #register").css("display","block");
          $(".tab-content #signin").css("display","none");
    });
    $(".page2-content__menu ul a").click(function(){
      $(".page2-content__menu ul a").removeClass("sort-active");
      $(this).addClass("sort-active");
      return false;
    });
    $('#votingbar').barrating({
      theme: 'fontawesome-stars',
    });
});